﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLab1
{
    class Ray
    {
        Vector2 org;   //Начальная точка
        Vector2 dir;   //Направление

        public Ray(Vector2 org, Vector2 dir)
        {
            this.org = org;
            dir.Normalize();
            this.dir = dir;
        }

        public Vector2 PointAtistance(float distance)
        {
            return org + dir * distance;
        }
    }
}
