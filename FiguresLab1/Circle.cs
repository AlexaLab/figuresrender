﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLab1
{
    class Circle : Figure
    {
        static public int localId;

        private float radius;
        private float diameter;
        private float radius_squared;

        public Circle()
        {
            localId++;
            id = "Circle " + localId.ToString();

            SetRadius(50.0f);
        }

        public void SetRadius(float radius)
        {
            this.radius = radius;
            diameter = radius * 2;
            radius_squared = radius * radius;
        }

        public override bool Test(float x, float y)
        {
            float dx = x - pos.x;
            float dy = y - pos.y;
            if (dx * dx + dy * dy <= radius_squared) return true;
            return false;
        }

        public override void Draw(Graphics g)
        {
            float x0 = pos.x - radius;
            float y0 = pos.y - radius;
            Pen p = Pens.Black;
            if (selected) p = Pens.Red;
            g.DrawEllipse(p, x0, y0, diameter, diameter);
        }

        public override float Sdf(Vector2 point)
        {
            return (pos - point).Length() - radius;
        }
    }
}
