﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FiguresLab1
{
    public partial class FormMain : Form
    {
        int old_x, old_y;
        List<Figure> listFigure = new List<Figure>();  //Динамический массив всех фигур

        int w, h;  //Ширина и высота рабочей области
        int padding = 25;  //Отступ

        Bitmap bitmap;

        public FormMain()
        {
            InitializeComponent();
        }

        Figure createFigure(string fig_type)    //Создание фигуры
        {
            switch (fig_type)   //Выбор типа фигуры
            {
                case "Circle": return new Circle();
                case "Square": return new Square();
            }
            return null;
        }

        private void pictureBoxMain_Paint(object sender, PaintEventArgs e)   //Перерисовка компонента
        {
            //e.Graphics.FillRectangle(Brushes.White, 0, 0, pictureBoxMain.Width, pictureBoxMain.Height);
            foreach (Figure fig in listFigure)
                fig.Draw(e.Graphics);
        }

        private void listBoxFigures_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (listBoxFigures.SelectedIndex == -1) return;  //Если фигкра не выбрана ничего не происходит
            //string sel = listBoxFigures.SelectedItem.ToString();   //Получение названия выбранной фигуры

            //foreach (Figure fig in listFigure)    //Отмена выделения всех фигур
            //{
            //    fig.selected = false;
            //}

            //for (int i = listFigure.Count - 1; i >= 0; i--)
            //{
            //    Figure fig = listFigure[i];

            //    if (fig.id == sel)
            //    {
            //        fig.selected = true;
            //        break;
            //    }
            //}

            //pictureBoxMain.Invalidate();   //Перерисовка компонента
        }

        private void pictureBoxMain_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (Figure fig in listFigure)    //Отмена выделения всех фигур
            {
                fig.selected = false;
                listBoxFigures.ClearSelected();
            }

            for (int i = listFigure.Count - 1; i >= 0; i--)     //Выделение цветом выбранной фигуры
            {
                Figure fig = listFigure[i];
                fig.selected |= fig.Test(e.X, e.Y);
                if (fig.selected == true)
                {
                    int index = listBoxFigures.FindString(fig.id);   //Выделение фигуры в listBox
                    listBoxFigures.SetSelected(index, true);
                    break;
                }
            }

            pictureBoxMain.Invalidate();
        }

        private void pictureBoxMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)   //ЛКМ
            {
                int dx = e.X - old_x;   //На сколько переместилась мышь по сравнению с предыдущими координатами
                int dy = e.Y - old_y;

                foreach (Figure fig in listFigure)
                {
                    if (!fig.selected) continue;   //Только для выделеных фигур
                    fig.pos.x += dx;    //Изменить позицию фигуры в соответствии с перемещением мыши 
                    fig.pos.y += dy;
                }

                pictureBoxMain.Invalidate();
            }

            old_x = e.X;   //Обновить координаты
            old_y = e.Y;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Figure fig = createFigure(comboBoxFigures.Text);    //Создание фигуры
            if (fig == null) return;   //Если фигура не создалась, ничего не происходит
            fig.pos.x = w * 0.5f;   //Позиция фигуры - центр pictureBox
            fig.pos.y = h * 0.5f;
            listBoxFigures.Items.Add(fig.id);   //Добавить фигуру в listBox
            listFigure.Add(fig);     //Добавить фигуру в listView
            pictureBoxMain.Invalidate();   //Перерисовка компонента
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            FormMain_Resize(null, null);
            bitmap = new Bitmap(w, h);
            pictureBoxMain.BackColor = Color.White;
            pictureBoxMain.Image = bitmap;

            comboBoxFigures.SelectedIndex = 0;
            comboBoxTypeRender.SelectedIndex = 0;
        }


        private void FormMain_Resize(object sender, EventArgs e)
        {
            w = ClientRectangle.Width - pictureBoxMain.Left - padding;    //Изменение размера pictureBox
            h = ClientRectangle.Height - pictureBoxMain.Top - padding;

            pictureBoxMain.Width = w;
            pictureBoxMain.Height = h;
            listBoxFigures.Height = h;

            //bitmap = new Bitmap(bitmap, new Size(w, h));
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            int i = 0;
            while (i < listFigure.Count)
            {
                if (listFigure[i].selected)   //Удаление выбранной фигуры с рисунка и из списка
                {
                    listBoxFigures.Items.Remove(listFigure[i].id);
                    listFigure.RemoveAt(i); 
                }
                i++;
            }

            pictureBoxMain.Invalidate();
        }

        private void buttonRender_Click(object sender, EventArgs e)
        {
            switch (comboBoxTypeRender.SelectedIndex)
            {
                case 0:    //Закрасить фигуру в её цвет

                    for (int y = 0; y < h; y++)
                    {
                        for (int x = 0; x < w; x++)
                        {
                            RGB color = new RGB();

                            foreach (Figure fig in listFigure)
                            {
                                if (fig.Test(x, y))
                                {
                                    color = fig.color;
                                }

                                bitmap.SetPixel(x, y, color.GetColor());
                            }
                        }
                    }

                    break;

                case 1:    //SDF

                    for (int y = 0; y < h; y++)
                    {
                        for (int x = 0; x < w; x++)
                        {
                            RGB color = new RGB();

                            Vector2 point = new Vector2(x, y);
                            float dMin = 100.0f;

                            foreach (Figure fig in listFigure)
                            {
                                float d = fig.Sdf(point);
                                if (d < dMin) dMin = d;
                            }

                            color = new RGB(dMin / 100.0f);

                            bitmap.SetPixel(x, y, color.GetColor());
                        }
                    }

                    break;

                case 2:    //SDF + цвет
                    
                    RGB c = new RGB();
                    c = RGB.GetRandom();

                    for (int y = 0; y < h; y++)
                    {
                        for (int x = 0; x < w; x++)
                        {
                            RGB color = new RGB();

                            Vector2 point = new Vector2(x, y);
                            float dMin = 100.0f;

                            foreach (Figure fig in listFigure)
                            {
                                color = c;

                                float d = fig.Sdf(point);
                                if (d < dMin) dMin = d;

                                float r = dMin / 100.0f + color.r;
                                r = (r > 1) ? 1 : ((r < 0) ? 0 : r);

                                float g = dMin / 100.0f + color.g;
                                g = (g > 1) ? 1 : ((g < 0) ? 0 : g);

                                float b = dMin / 100.0f + color.b;
                                b = (b > 1) ? 1 : ((b < 0) ? 0 : b);

                                color = new RGB(r, g, b);

                                bitmap.SetPixel(x, y, color.GetColor());
                            }
                        }
                    }

                    break;
            }

            pictureBoxMain.Invalidate();
        }
    }
}
