﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLab1
{
    class Figure
    {
        public Vector2 pos = new Vector2(1, 1);
        public bool selected;

        public string id;

        public RGB color = new RGB(1, 1, 0);

        public Figure()
        {
            color = RGB.GetRandom();
        }

        virtual public bool Test(float x, float y)
        {
            return false;
        }

        virtual public void Draw(Graphics g)
        {

        }

        virtual public float Sdf (Vector2 point)
        {
            return 0;
        }
    }
}
