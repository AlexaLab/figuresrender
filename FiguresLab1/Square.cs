﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLab1
{
    class Square : Figure
    {
        static public int localId;

        private float side;
        private float half_side;

        public Square()
        {
            localId++;
            id = "Square " + localId.ToString();
            SetSide(100.0f);
        }

        public void SetSide(float side)
        {
            this.side = side;
            half_side = side * 0.5f;
        }

        public override bool Test(float x, float y)
        {
            float xmin = pos.x - half_side;
            float ymin = pos.y - half_side;
            float xmax = pos.x + half_side;
            float ymax = pos.y + half_side;
            if (x < xmin || y < ymin) return false;
            if (x > xmax || y > ymax) return false;
            return true;
        }

        public override void Draw(Graphics g)
        {
            float x0 = pos.x - half_side;
            float y0 = pos.y - half_side;
            Pen p = Pens.Black;
            if (selected) p = Pens.Red;
            g.DrawRectangle(p, x0, y0, side, side);
        }

        public override float Sdf(Vector2 point)
        {
            Vector2 d = (point - pos).Abs() - half_side;

            float inner_d = Math.Min(Math.Max(d.x, d.y), 0.0f);
            d.Max(0.0f);
            float outer_d = d.Length();

            return inner_d + outer_d;
        }
    }
}
