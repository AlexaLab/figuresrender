﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLab1
{
    class RGB
    {
        public float r, g, b;

        public RGB()
        {
            r = g = b = 1;   //По умолчанию создаётся белый цвет
        }

        public RGB(float x)
        {
            r = g = b = x;   //Серый
        }

        public RGB(float r, float g, float b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public static RGB GetRandom()
        {
            Random random = new Random();
            RGB randomColor = new RGB((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
            return randomColor;
        }

        public static RGB operator +(RGB a, RGB b)
        {
            RGB newColor = new RGB();
            newColor.r = a.r + b.r;
            newColor.g = a.g + b.g;
            newColor.b = a.b + b.b;
            return newColor;
        }

        public static RGB operator *(RGB a, float x)
        {
            RGB newColor = new RGB();
            newColor.r = a.r * x;
            newColor.g = a.g * x;
            newColor.b = a.b * x;
            return newColor;
        }

        public Color GetColor()
        {
            int r = (int)(this.r * 255.0f);
            r = (r > 255) ? 255 : ((r < 0) ? 0 : r);   //Проверка выхода за границы

            int g = (int)(this.g * 255.0f);
            g = (g > 255) ? 255 : ((g < 0) ? 0 : g);

            int b = (int)(this.b * 255.0f);
            b = (b > 255) ? 255 : ((b < 0) ? 0 : b);

            Color color = new Color();
            color = Color.FromArgb(r, g, b);
            return color;
        }
    }
}
