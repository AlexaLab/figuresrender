﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FiguresLab1
{
    class Vector2
    {
        public float x, y;

        public Vector2()
        {

        }

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static Vector2 FromPolar(float angle)
        {
            Vector2 vector2 = new Vector2();
            angle = angle * (float)Math.PI / 180.0f;
            vector2.x = (float)Math.Cos(angle);
            vector2.y = (float)Math.Sin(angle);
            return vector2;
        }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            Vector2 newVector2 = new Vector2();
            newVector2.x = a.x + b.x;
            newVector2.y = a.y + b.y;
            return newVector2;
        }

        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            Vector2 newVector2 = new Vector2();
            newVector2.x = a.x - b.x;
            newVector2.y = a.y - b.y;
            return newVector2;
        }

        public static Vector2 operator -(Vector2 a, float x)
        {
            Vector2 newVector2 = new Vector2();
            newVector2.x = a.x - x;
            newVector2.y = a.y - x;
            return newVector2;
        }

        public static Vector2 operator *(Vector2 a, float x)
        {
            Vector2 newVector2 = new Vector2();
            newVector2.x = a.x * x;
            newVector2.y = a.y * x;
            return newVector2;
        }

        public float Length()
        {
            return (float)Math.Sqrt(x * x + y * y);
        }

        public Vector2 Abs()
        {
            return new Vector2(Math.Abs(x), Math.Abs(y));
        }

        public void Max(float v)
        {
            x = Math.Max(x, v);
            y = Math.Max(y, v);
        }

        public void Normalize()
        {
            float length = Length();
            x /= length;
            y /= length;
        }
    }
}
